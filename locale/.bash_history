#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:contact_tracing_ptis.entry_registration,age:"
msgid "Age"
msgstr "Edad"

msgctxt "field:contact_tracing_ptis.entry_registration,case_status:"
msgid "Case status"
msgstr "Estado del caso"

msgctxt "field:contact_tracing_ptis.entry_registration,cellphone:"
msgid "Cellphone"
msgstr "Telefono"

msgctxt "field:contact_tracing_ptis.entry_registration,citizenship:"
msgid "Nationality"
msgstr "Nacionalidad"

msgctxt "field:contact_tracing_ptis.entry_registration,city:"
msgid "City"
msgstr "Ciudad"

msgctxt "field:contact_tracing_ptis.entry_registration,department:"
msgid "Department"
msgstr "Departamento"

msgctxt "field:contact_tracing_ptis.entry_registration,dob:"
msgid "Date of birth"
msgstr "Fecha de nacimiento"

msgctxt "field:contact_tracing_ptis.entry_registration,email:"
msgid "Email"
msgstr "Email"

msgctxt "field:contact_tracing_ptis.entry_registration,first_consult_date:"
msgid "First consult date"
msgstr "Fecha de primer consulta"

msgctxt ""
"field:contact_tracing_ptis.entry_registration,first_consult_institution:"
msgid "First consult institution"
msgstr "Institución de priemr consulta"

msgctxt "field:contact_tracing_ptis.entry_registration,floor:"
msgid "Floor"
msgstr "Piso"

msgctxt "field:contact_tracing_ptis.entry_registration,gender:"
msgid "Gender"
msgstr "Genero"

msgctxt "field:contact_tracing_ptis.entry_registration,house_number:"
msgid "House Number"
msgstr "Numero"

msgctxt "field:contact_tracing_ptis.entry_registration,id_case:"
msgid "ID Case"
msgstr "ID caso"

msgctxt "field:contact_tracing_ptis.entry_registration,institution:"
msgid "Institution"
msgstr "Institución"

msgctxt "field:contact_tracing_ptis.entry_registration,institution_role:"
msgid "Role inside the system"
msgstr "Rol en la institución"

msgctxt "field:contact_tracing_ptis.entry_registration,neighboard:"
msgid "Neighboard"
msgstr "Barrio"

msgctxt "field:contact_tracing_ptis.entry_registration,notification_date:"
msgid "Notification Date"
msgstr "Fecha de notificación"

msgctxt "field:contact_tracing_ptis.entry_registration,patient_city:"
msgid "City"
msgstr "Ciudad"

msgctxt "field:contact_tracing_ptis.entry_registration,patient_condition:"
msgid "Patient Condition"
msgstr "Condición"

msgctxt "field:contact_tracing_ptis.entry_registration,patient_phone:"
msgid "Phone"
msgstr "Telefono"

msgctxt "field:contact_tracing_ptis.entry_registration,patient_province:"
msgid "Province"
msgstr "Provincia"

msgctxt "field:contact_tracing_ptis.entry_registration,province:"
msgid "Province"
msgstr "Provincia"

msgctxt "field:contact_tracing_ptis.entry_registration,receiving_user:"
msgid "User"
msgstr "Usuario que registra la primer atención"

msgctxt "field:contact_tracing_ptis.entry_registration,ref:"
msgid "PUID"
msgstr "DNI"

msgctxt "field:contact_tracing_ptis.entry_registration,register_date:"
msgid "Register date"
msgstr "Fecha de registro"

msgctxt "field:contact_tracing_ptis.entry_registration,street:"
msgid "Street"
msgstr "Calle"

msgctxt "field:contact_tracing_ptis.entry_registration,user_:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:contact_tracing_ptis.entry_registration,zip_code:"
msgid "Zip code"
msgstr "CP"

msgctxt "field:contact_tracing_ptis.request,address:"
msgid "Address"
msgstr "Dirección"

msgctxt "field:contact_tracing_ptis.request,address_domain:"
msgid "Address domain"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,age:"
msgid "Age"
msgstr "Edad"

msgctxt "field:contact_tracing_ptis.request,appointment_date:"
msgid "Turno"
msgstr "Turno"

msgctxt "field:contact_tracing_ptis.request,appointment_institution:"
msgid "Institución turno"
msgstr "Institución"

msgctxt "field:contact_tracing_ptis.request,dni:"
msgid "DNI"
msgstr "DNI"

msgctxt "field:contact_tracing_ptis.request,dni_type:"
msgid "Tipo de documento"
msgstr "Tipo de documento"

msgctxt "field:contact_tracing_ptis.request,dob:"
msgid "Fecha de nacimiento"
msgstr "Fecha de naciemiento"

msgctxt "field:contact_tracing_ptis.request,has_dni:"
msgid "Tenencia de DNI"
msgstr "Tiene DNI"

msgctxt "field:contact_tracing_ptis.request,nickname:"
msgid "Apodo/sobrenombre"
msgstr "Apodo/sobrenombre"

msgctxt "field:contact_tracing_ptis.request,observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:contact_tracing_ptis.request,requesting_institution:"
msgid "Requesting institutión"
msgstr "Institución solicitante"

msgctxt "field:contact_tracing_ptis.request,requesting_user:"
msgid "Requesting user"
msgstr "Usuario solicitante"

msgctxt "field:contact_tracing_ptis.request,state:"
msgid "Estado de la solicitid"
msgstr "Estado de solicitud"

msgctxt "field:contact_tracing_ptis.request,state_icon:"
msgid "state_icon"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,cellphone:"
msgid "Institution cellphone"
msgstr "Telefono"

msgctxt "help:contact_tracing_ptis.entry_registration,email:"
msgid "Institution email"
msgstr "Email"

msgctxt "help:contact_tracing_ptis.entry_registration,institution:"
msgid "Institution that notifies"
msgstr "Institución"

msgctxt "help:contact_tracing_ptis.entry_registration,institution_role:"
msgid "Administrativo, psicologo, tallerista, etc"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,patient_phone:"
msgid "Patient phone"
msgstr "Telefono"

msgctxt "help:contact_tracing_ptis.entry_registration,patient_province:"
msgid "Patient Province"
msgstr "Provincia"

msgctxt "model:contact_tracing_ptis.entry_registration,name:"
msgid "Entry registration"
msgstr ""

msgctxt "model:contact_tracing_ptis.request,name:"
msgid "Request for attention"
msgstr ""

msgctxt "model:ir.action,name:act_entry_registration_open"
msgid "Entry registration"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,case_status:"
msgid "Urgente"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,gender:"
msgid "Female"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,gender:"
msgid "Male"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,patient_condition:"
msgid "Ambulatory"
msgstr ""

#, fuzzy
msgctxt "selection:contact_tracing_ptis.entry_registration,patient_condition:"
msgid "Inpatient"
msgstr "Condición"

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "CI"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "DNI"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "DNI E (extranjeros)"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "Identificación extranjera"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "LC"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "LE"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "Pasaporte"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,has_dni:"
msgid "NS/NC"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,has_dni:"
msgid "No tiene"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,has_dni:"
msgid "Tiene"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Assigned"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Attended the appointment"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Did not attend the appointment"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Expecting"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "CASE/EVENT IDENTIFICATION"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "CLASSIFICATION AT THE NOTIFICATION MOMENT"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "CLINICAL INFORMATION"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "INSTITUTION IDENTIFICATION"
msgstr ""
