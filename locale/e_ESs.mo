#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:contact_tracing_ptis.contact_tracing_call,alert:"
msgid "Alert"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,awareness:"
msgid "Awareness"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,caller_tracing:"
msgid "Caller"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,date:"
msgid "Date"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,evolution:"
msgid "How do you consider your health since yesterday?"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,evolution_number:"
msgid "Evolution"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,has_signs:"
msgid "Signs"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "How do you consider your health today?"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,name:"
msgid "Contact Tracing"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,notes:"
msgid "Notes"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,pain:"
msgid "Pain"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,status:"
msgid "Status"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,symptoms:"
msgid "Symptoms Check"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,verbal:"
msgid "Verbal"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_call,warning_icon:"
msgid "Warning Call"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,age:"
msgid "Age"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,call_icon:"
msgid "Call icon"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,contact_tracings:"
msgid "Contacts Tracings"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,context_list:"
msgid "Context List"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,context_selection:"
msgid "Context"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,critical_info:"
msgid "Critical Info"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,discharge:"
msgid "Person Discharge"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,discharge_date:"
msgid "Discharge Date"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,entry_registration:"
msgid "Mandatory notification disease"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,first_caller:"
msgid "Caller"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,first_contact:"
msgid "First Contact Date"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,geo_ref:"
msgid "Geo Ref"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,health_prof:"
msgid "Health Professional"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,institutions:"
msgid "Institutions"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,members_reached:"
msgid "Members on du"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,members_unreached:"
msgid "Members on du"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,phone:"
msgid "Phone"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,responsible_users:"
msgid "Responsible Users"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,result:"
msgid "Result"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,state:"
msgid "State"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,swabbing_date:"
msgid "Swabbing date"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,swabbing_result:"
msgid "Swabbing result"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,urladdr2:"
msgid "Google Maps"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis,urladdr:"
msgid "OSM Map"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.contact_tracing_ptis-party.party,contact_tracing:"
msgid "Contact Tracing"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis-party.party,party:"
msgid "Party"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.contact_tracing_ptis-res.user,contact_tracing:"
msgid "Contact Tracing"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis-res.user,user:"
msgid "User"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis.members,party:"
msgid "Party"
msgstr ""

msgctxt "field:contact_tracing_ptis.contact_tracing_ptis.symptoms_check,name:"
msgid "Contact Tracing"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.contact_tracing_ptis.symptoms_check,symptom:"
msgid "Symptom"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,address:"
msgid "Dirección"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,ae:"
msgid "Recibe Asignación por Embarazo para proteccion social"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,afc:"
msgid "Recibe Asignación Familiar por Conyugue"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,age:"
msgid "Age"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,age_children:"
msgid "Edad de los hijos"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,ap:"
msgid "Recibe Asignación Provincial"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,asigned_disability:"
msgid "Discapacidad asignada"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,aud:"
msgid "Recibe Asignación Universal por Discapacidad"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,auh:"
msgid "Recibe AUH"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,birth_sex:"
msgid "Sexo biologico"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,case_status:"
msgid "Case status"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,cellphone:"
msgid "Telefono"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,cellphone_3:"
msgid "Cellphone"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,citizenship:"
msgid "Nationality"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,city:"
msgid "City"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,civil_status:"
msgid "Estado civil"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,coexist:"
msgid "Convice con sus hijos"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,cud:"
msgid "Posee certificado unico de discapacidad"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,cultural_info:"
msgid "Informacion cultural"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,department:"
msgid "Department"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.entry_registration,difficulties_or_limitations:"
msgid "Dificultades o limitaciones"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,dni:"
msgid "DNI"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,dni_type:"
msgid "Tipo de DNI"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,dob:"
msgid "Date of birth"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,email:"
msgid "Email"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,email_3:"
msgid "Email"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,employment:"
msgid "Trabaja"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,employment_situation:"
msgid "Situacion laboral"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,entry_date:"
msgid "Entry Date"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,first_consult_date:"
msgid "First consult date"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.entry_registration,first_consult_institution:"
msgid "First consult institution"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,floor:"
msgid "Floor"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,gender:"
msgid "Gender"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,gestational_age:"
msgid "Edad gestacional en meses"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,has_children:"
msgid "Tiene hijos"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,has_dni:"
msgid "Tiene DNI"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,health_coverage:"
msgid "Covertura de salud"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,house_number:"
msgid "House Number"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,housing:"
msgid "Condición de la vivienda"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,id_case:"
msgid "ID Case"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,informal_education:"
msgid "Educación informal"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,institution:"
msgid "Institution"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,institution_role:"
msgid "Role inside the system"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,is_pregnant:"
msgid "Es persona gestante"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Maximo nivel de educacion formal alcanzado"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,link_tc:"
msgid "Vinculo con el contacto de confianza"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,mail_tc:"
msgid "Mail del contacto de confianza"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,name_3:"
msgid "Name 3ro"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,name_health_coverage:"
msgid "Name health coverage"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,neighboard:"
msgid "Neighboard"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,notification_date:"
msgid "Notification Date"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,number_children:"
msgid "Cantidad de hijos"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,observations_3:"
msgid "Observaciones persona acompañante"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,other_income:"
msgid "Recibe otros ingresos"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,p7:"
msgid "Pensión por 7 hijos o mas"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,patient_city:"
msgid "City"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,patient_condition:"
msgid "Patient Condition"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,patient_phone:"
msgid "Phone"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,patient_province:"
msgid "Province"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,pd2:"
msgid "Prestación por desempleo"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,pd:"
msgid "Recibe Pensión por Discapacidad o Invalidez"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,person_live_with:"
msgid "Persona con la que convivio los ultimo 30 dias"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,presentation:"
msgid "Presentation"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,progesar:"
msgid "Recibe PROGRESAR"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,progresar:"
msgid "Recibe PROGRESAR"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,province:"
msgid "Province"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,re_entry_date:"
msgid "Re Entry Date"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,read:"
msgid "Lee?"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.entry_registration,reasons_for_incompleteness:"
msgid "Motivos de incompletitud del trayecto educativo"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,receiving_user:"
msgid "User"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,ref:"
msgid "PUID"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,register_date:"
msgid "Register date"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,registration_date:"
msgid "Notification Date"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.entry_registration,retirement_contributions:"
msgid "Realiza aportes jubilatorios"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,ri:"
msgid "Recibe Retiro por invalidez o desempleo"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,salary:"
msgid "Salario"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,se:"
msgid ""
"Recibe Régimen de reparación económica para las niñas, niños y adolescentes "
"Ley 27.452"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,significant_referent:"
msgid "Consigna datos de referente significativo"
msgstr ""

msgctxt ""
"field:contact_tracing_ptis.entry_registration,significant_referent_st:"
msgid "Referente significativo"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,street:"
msgid "Street"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,trusted_contact:"
msgid "Contacto de confianza"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,tutor:"
msgid "Padre/Madre/Tutor/a o Referente"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,tutor_v:"
msgid "Vinculo con el referente"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,type_of_housing:"
msgid "Poseción de la vivienda"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,user_:"
msgid "Patient"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,what_c_h:"
msgid "Tipo de covertura de salud"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,work_experience:"
msgid "Experiencias laborales u oficios"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,write:"
msgid "Escribe"
msgstr ""

msgctxt "field:contact_tracing_ptis.entry_registration,zip_code:"
msgid "Zip code"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,address:"
msgid "Address"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,address_domain:"
msgid "Address domain"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,age:"
msgid "Age"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,appointment_date:"
msgid "Turno"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,appointment_institution:"
msgid "Institución turno"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,dni:"
msgid "DNI"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,dni_type:"
msgid "Tipo de documento"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,dob:"
msgid "Fecha de nacimiento"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,has_dni:"
msgid "Tenencia de DNI"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,nickname:"
msgid "Apodo/sobrenombre"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,observations:"
msgid "Observations"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,requesting_institution:"
msgid "Requesting institutión"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,requesting_user:"
msgid "Requesting user"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,state:"
msgid "Estado de la solicitid"
msgstr ""

msgctxt "field:contact_tracing_ptis.request,state_icon:"
msgid "state_icon"
msgstr ""

msgctxt "field:gnuhealth.contact_tracing,entry_registration:"
msgid "Mandatory notification disease"
msgstr ""

msgctxt "help:contact_tracing_ptis.contact_tracing_call,evolution:"
msgid "Evolution"
msgstr ""

msgctxt "help:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "Instrospective sense of health"
msgstr ""

msgctxt "help:contact_tracing_ptis.contact_tracing_call,status:"
msgid ""
"Unreached: The contact has not been reached yet.\n"
"Following up: The contact has been traced, demographics information has been created and followup evaluations status are stored in the evaluations"
msgstr ""

msgctxt "help:contact_tracing_ptis.contact_tracing_ptis,responsible_users:"
msgid "Users that can access the record"
msgstr ""

msgctxt "help:contact_tracing_ptis.contact_tracing_ptis,result:"
msgid "Discharge result Discharge Result"
msgstr ""

msgctxt "help:contact_tracing_ptis.contact_tracing_ptis,urladdr2:"
msgid "Locates the DU on Google Maps"
msgstr ""

msgctxt "help:contact_tracing_ptis.contact_tracing_ptis,urladdr:"
msgid "Locates the DU on the Open Street Map by default"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,cellphone:"
msgid "Institution cellphone"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,cellphone_3:"
msgid "Institution cellphone"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,email:"
msgid "Institution email"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,email_3:"
msgid "Institution email"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,institution:"
msgid "Institution that notifies"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,institution_role:"
msgid "Administrativo, psicologo, tallerista, etc"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,patient_phone:"
msgid "Patient phone"
msgstr ""

msgctxt "help:contact_tracing_ptis.entry_registration,patient_province:"
msgid "Patient Province"
msgstr ""

msgctxt "model:contact_tracing_ptis.contact_tracing_call,name:"
msgid "Person Contact Tracing"
msgstr ""

msgctxt "model:contact_tracing_ptis.contact_tracing_ptis,name:"
msgid "First Contacts with the Person"
msgstr ""

msgctxt "model:contact_tracing_ptis.contact_tracing_ptis-party.party,name:"
msgid "Contact Tracing Member"
msgstr ""

msgctxt "model:contact_tracing_ptis.contact_tracing_ptis-res.user,name:"
msgid "Contact Tracing - Health Professional"
msgstr ""

msgctxt "model:contact_tracing_ptis.contact_tracing_ptis.members,name:"
msgid "Contact Tracing Members"
msgstr ""

msgctxt "model:contact_tracing_ptis.contact_tracing_ptis.symptoms_check,name:"
msgid "Person Symptoms"
msgstr ""

msgctxt "model:contact_tracing_ptis.entry_registration,name:"
msgid "Entry registration"
msgstr ""

msgctxt "model:contact_tracing_ptis.request,name:"
msgid "Request for attention"
msgstr ""

msgctxt "model:ir.action,name:act_contact_tracing_ptis_open"
msgid "Contact Tracing"
msgstr ""

msgctxt "model:ir.action,name:act_entry_registration_open"
msgid "Entry registration"
msgstr ""

msgctxt "model:ir.ui.menu,name:contact_tracing_ptis_menu"
msgid "Seguimiento de trayectorias"
msgstr ""

msgctxt "model:ir.ui.menu,name:entry_registration_menu"
msgid "Gestión de intervenciones"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_call,evolution:"
msgid "Improving"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_call,evolution:"
msgid "Not answered"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_call,evolution:"
msgid "Status Quo"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_call,evolution:"
msgid "Worsening"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "Bad"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "Excelent"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "Good"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "Not answered"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "Regular"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.contact_tracing_call,introspective_health:"
msgid "Very good"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_call,status:"
msgid "Following up"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_call,status:"
msgid "Not available"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_call,status:"
msgid "Unreached"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,call_icon:"
msgid "Already call"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,call_icon:"
msgid "To call"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,call_icon:"
msgid "Tracing done"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,geo_ref:"
msgid "Georef"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,geo_ref:"
msgid "No georef"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,result:"
msgid "death person"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,result:"
msgid "discarded by laboratory"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,result:"
msgid "hospitalized"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,result:"
msgid "recuperated"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,result:"
msgid "tracing lossed"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,state:"
msgid "Done"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,state:"
msgid "In process"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,swabbing_result:"
msgid "+"
msgstr ""

msgctxt "selection:contact_tracing_ptis.contact_tracing_ptis,swabbing_result:"
msgid "-"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,birth_sex:"
msgid "Female"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,birth_sex:"
msgid "Male"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,case_status:"
msgid "Urgente"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,civil_status:"
msgid "Casado"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,civil_status:"
msgid "Divorciado"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,civil_status:"
msgid "Soltero"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,coexist:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,coexist:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,cud:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,cud:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,dni_type:"
msgid "DNI"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,dni_type:"
msgid "DU"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,employment:"
msgid "No trabaja, ni busca trabajo"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,employment:"
msgid "No trabaja, pero busca trabajo"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,employment:"
msgid "Trabaja"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,employment:"
msgid ""
"Transita licencia por vaciones o enfermedad, suspencion con pago, conflicto "
"laboral, etc"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,employment_situation:"
msgid "No trabaja, ni busca trabajo"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,employment_situation:"
msgid "No trabaja, pero busca trabajo"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,employment_situation:"
msgid "Trabaja"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,employment_situation:"
msgid ""
"Transita licencia por vaciones o enfermedad, suspencion con pago, conflicto "
"laboral, etc"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,gender:"
msgid "Female"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,gender:"
msgid "Male"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,has_children:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,has_children:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,has_dni:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,has_dni:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,health_coverage:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,health_coverage:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,housing:"
msgid "Alquila"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,housing:"
msgid "Cedida por trabajo"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,housing:"
msgid "Otra situación"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,housing:"
msgid "Prestada"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,housing:"
msgid "Propietario"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,informal_education:"
msgid "No"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,informal_education:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,is_pregnant:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,is_pregnant:"
msgid "Si"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Inicial"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "No completo educación"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Ns/Nr"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Primario"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Primario incompleto"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Secundario"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Secundario incompleto"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Sin instruccion formal"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Superior, Terciario o Universitario"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,level_max_education:"
msgid "Superior, Terciario o Universitario incompleto"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,other_income:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,other_income:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,patient_condition:"
msgid "Ambulatory"
msgstr ""

#, fuzzy
msgctxt "selection:contact_tracing_ptis.entry_registration,patient_condition:"
msgid "Inpatient"
msgstr "Condición"

msgctxt "selection:contact_tracing_ptis.entry_registration,presentation:"
msgid "Acompañado"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,presentation:"
msgid "Consulta un tercero"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,presentation:"
msgid "Solo"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,read:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,read:"
msgid "Si"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,reasons_for_incompleteness:"
msgid "Consumo de sustancias"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,reasons_for_incompleteness:"
msgid "Economicos"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,reasons_for_incompleteness:"
msgid "Esta cursando actualmente"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,reasons_for_incompleteness:"
msgid "Familiares"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,reasons_for_incompleteness:"
msgid "Otros"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,reasons_for_incompleteness:"
msgid "Personales y afectivos"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,retirement_contributions:"
msgid "No"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,retirement_contributions:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,salary:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,salary:"
msgid "Si"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,significant_referent:"
msgid "No"
msgstr ""

msgctxt ""
"selection:contact_tracing_ptis.entry_registration,significant_referent:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,type_of_housing:"
msgid "Casa"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,type_of_housing:"
msgid "Casilla"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,type_of_housing:"
msgid "Departamento"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,type_of_housing:"
msgid "Persona viviendo en la calle"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,type_of_housing:"
msgid "Rancho"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,what_c_h:"
msgid "Obra Social"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,what_c_h:"
msgid "Prepaga"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,write:"
msgid "No"
msgstr ""

msgctxt "selection:contact_tracing_ptis.entry_registration,write:"
msgid "Si"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "CI"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "DNI"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "DNI E (extranjeros)"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "Identificación extranjera"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "LC"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "LE"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,dni_type:"
msgid "Pasaporte"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,has_dni:"
msgid "NS/NC"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,has_dni:"
msgid "No tiene"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,has_dni:"
msgid "Tiene"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Assigned"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Attended the appointment"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Did not attend the appointment"
msgstr ""

msgctxt "selection:contact_tracing_ptis.request,state:"
msgid "Expecting"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "CASE/EVENT IDENTIFICATION"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "CLASSIFICATION AT THE NOTIFICATION MOMENT"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "CLINICAL INFORMATION"
msgstr ""

msgctxt "view:contact_tracing_ptis.entry_registration:"
msgid "INSTITUTION IDENTIFICATION"
msgstr ""
