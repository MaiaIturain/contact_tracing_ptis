# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSQL, fields
from dateutil.relativedelta import relativedelta
from trytond.pyson import Eval, Bool, Or
from datetime import datetime, timedelta, date
from trytond.wizard import Button
from trytond.pool import Pool
from trytond.transaction import Transaction

class EntryRegistration(ModelSQL,ModelView):
    'Entry registration'
    __name__ = 'contact_tracing_ptis.entry_registration'
   
    '''
    ------------------Institution identification y a la persona que lo recibe-------------------------------
    '''
    institution = fields.Many2One('gnuhealth.institution','Institución',
        help="Institución que recibe",
        required=True)
    entry_date = fields.Date('Fecha de ingreso', 
        required=True)
    registration_date = fields.Date('Fecha de registro',
        required=True)
    receiving_user = fields.Many2One('res.user', 'Usuario',
                readonly=True)
    presentation = fields.Selection([
        (None, ''),    
        ('solo', 'Solo'),
        ('acompañado', 'Acompañado'),
        ('tercero', 'Consulta un tercero'),
        ],'Como llega a la institución')
    estado = fields.Selection([
        (None,''),
        ('seguir','Crear Seguimiento'),
        ('siguiendo','Siguiendo'),
        ],'Estado')
    '''
    ------------------Datos tercero-------------------------------
    '''
    cellphone_3 = fields.Char('Telefono',
        help="Telefono del tercero, en caso de no poseer pone rel de la CAA")
    name_3 = fields.Char('Nombre', help="Nombre del tercero que se presenta")
    observations_3 = fields.Char('Observaciones',
        help="Observaciones de la persona acompañante sobre situaciones de consumo")

    '''
    ------------------Datos referidos a la persona consultante------------------------------
    '''
    user_ = fields.Many2One('party.party','Solicitante',
        domain= [('is_person','=',True)],
        required=True)
    has_dni = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No')],'Tiene DNI')
    dni_type = fields.Selection([
        (None, ''),    
        ('du', 'DU'),
        ('dni', 'DNI'),
        ],'Tipo de DNI')
    dni = dni = fields.Function(fields.Char('DNI/CUIT'), 'on_change_with_dni')
    dob = fields.Function(fields.Date('Fecha de nacimiento'), 'on_change_with_dob')
    citizenship = fields.Many2One('country.country','Nacionalidad')
    address = fields.Many2One('party.address',
        'Dirección',readonly=True,
        domain=[
            ('id', 'in', Eval('address_domain')),
            ('party', '=', Eval('user_'))
            ])
    # este es el dominio de address.
    address_domain = fields.Function(
        fields.Many2Many('party.address', None, None, 'Address domain'),
        'on_change_with_address_domain')
    cellphone = fields.Function(
        fields.Char('Telefono',required=True),
        'on_change_with_cellphone')
    age = fields.Function(fields.Integer('Edad'), 'on_change_with_age')
    birth_sex = fields.Selection([
        (None,''),
        ('m','Masculino'),
        ('f','Femenino'),
        ],'Sexo biológico')
    gender = fields.Selection([
        (None,''),
        ('m','Masculino'),
        ('f','Femenino'),
        ('mt','Mujer trans/travesti'),
        ('vt','Varon trans'),
        ('o','Otro'),
        ],'Género')
    civil_status = fields.Selection([
        (None,''),
        ('c','Casado'),
        ('s','Soltero'),
        ('d','Divorciado'),
        ],'Estado civil')
    has_children = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Tiene hijos')
    number_children = fields.Char('Cantidad de hijos')
    age_children = fields.Char('Edad de los hijos')
    coexist = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Convive con sus hijos')
    tutor = fields.Char('Padre/Madre/Tutor/a o Referente')
    tutor_v = fields.Char('Vinculo con el referente')
    '''
    ------------------Datos referidos a la persona consultante - Salud------------------------------
    '''
    health_coverage = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Cobertura de salud')
    what_c_h = fields.Selection([
        (None, ''),    
        ('os', 'Obra Social'),
        ('p', 'Prepaga'),
        ],'Tipo de cobertura de salud')
    name_health_coverage = fields.Char('Nombre de la cobertura de salud')
    is_pregnant = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Si es persona gestante, cursa embarazo actual?')
    gestational_age = fields.Char('Edad gestacional en meses')
    '''
    ------------------Datos de persona de confianza-------------------------------
    '''
    cellphone_tc = fields.Char('Teléfono de contacto de confianza')
    trusted_contact = fields.Char('Nombre de contacto de confianza')
    link_tc = fields.Char('Vinculo con el contacto de confianza')
    mail_tc = fields.Char('Mail del contacto de confianza')
    
    '''
    ------------------Datos referidos a la persona consultante - Identidad cultural------------------------------
    '''
    cultural_info = fields.Text('Información cultural')
    '''
    ------------------Datos referidos a la persona consultante - Educación------------------------------
    '''
    lee = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Lee?')
    write = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Escribe?')
    level_max_education = fields.Selection([
        (None, ''),   
        ('i', 'Inicial'),
        ('p', 'Primario'),
        ('pi', 'Primario incompleto'),
        ('s', 'Secundario'),
        ('si', 'Secundario incompleto'),
        ('t', 'Superior, Terciario o Universitario'),
        ('ti', 'Superior, Terciario o Universitario incompleto'),
        ('sif', 'Sin instrucción formal'),
        ('nce', 'No completo educación'),
        ('nsnc', 'Ns/Nr'),
        ],'Máximo nivel de educación formal alcanzado')
    reasons_for_incompleteness = fields.Selection([
        (None, ''),    
        ('ec', 'Económicos'),
        ('fam', 'Familiares'),
        ('per', 'Personales y afectivos'),
        ('cons', 'Consumo de sustancias'),
        ('cursa', 'Esta cursando actualmente'),
        ('otro', 'Otros'),
        ],'Motivos de incompletitud del trayecto educativo')
    informal_education = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Educación informal')
    '''
    ------------------Datos referidos a la persona consultante - Situación laboral------------------------------
    '''
    employment = fields.Selection([
        (None, ''),
        ('t', 'Trabaja'),
        ('nt', 'No trabaja, pero busca trabajo'),
        ('nonbt', 'No trabaja, ni busca trabajo'),
        ('lic', 'Transita licencia por vacaciones o enfermedad, suspensión con pago, conflicto laboral, etc'),
        ],'Trabaja')
    salary = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Salario en moneda')
    employment_situation = fields.Selection([
        (None, ''),
        ('e', 'Empleado/a u obrero/a'),
        ('o', 'Trabajador/a por cuenta propia sin personal a cargo'),
        ('cp', 'Trabajador/a por cuenta propia con personal a cargo'),
        ('tf', 'Trabajador/a familiar'),
        ],'Situacion laboral')
    retirement_contributions = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Realiza aportes jubilatorios (empleador)')
    own_retirement_contributions = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Realiza aportes jubilatorios de manera independiente')
    work_experience = fields.Char('Experiencias laborales u oficios')
    other_income = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Recibe otros ingresos')
    auh = fields.Boolean('Recibe AUH')
    aud = fields.Boolean('Recibe Asignación Universal por Discapacidad')
    pd = fields.Boolean('Recibe Pensión por Discapacidad o Invalidez')
    p7 = fields.Boolean('Pensión por 7 hijos o mas')
    ri = fields.Boolean('Recibe Retiro por invalidez o desempleo')
    se = fields.Boolean('Ley 27452',help="Recibe Régimen de reparación económica para las niñas, niños y adolescentes Ley 27.452")
    progresar = fields.Boolean('Recibe PROGRESAR')
    pd2 = fields.Boolean('Prestación por desempleo')
    ap = fields.Boolean('Recibe Asignación Provincial')
    afc = fields.Boolean('Recibe Asignación Familiar por Conyugue')
    ae = fields.Boolean('Recibe Asignación por Embarazo')
    # Aca faltan mas opciones para completar
    '''
    ------------------Datos referidos a la persona consultante - Vivienda------------------------------
    '''
    housing = fields.Selection([
        (None, ''),    
        ('p', 'Propietario'),
        ('a', 'Alquila'),
        ('cpt', 'Cedida por trabajo'),
        ('prest', 'Prestada'),
        ('otra', 'Otra situación'),
        ],'Condición de la vivienda')
    type_of_housing = fields.Selection([
        (None, ''),    
        ('casa', 'Casa'),
        ('rancho', 'Rancho'),
        ('casilla', 'Casilla'),
        ('dpto', 'Departamento'),
        ('calle', 'Persona viviendo en la calle'),
        ],'Poseción de la vivienda')
    '''
    ------------------Datos referidos a la persona consultante - Situación familiar y social------------------------------
    '''
    person_live_with = fields.Char('Persona con la que convivió los ultimo 30 días')
    significant_referent = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Consigna datos de referente significativo')
    significant_referent_st = fields.Char('Referente significativo')
    '''
    ------------------Datos referidos a la persona consultante - Discapacidad------------------------------
    '''
    cud = fields.Selection([
        (None, ''),    
        ('si', 'Si'),
        ('no', 'No'),
        ],'Posee certificado único de discapacidad')
    asigned_disability = fields.Selection([
        (None, ''),
        ('mental','Mental'),
        ('motora','Motora'),
        ('visceral','Visceral'),
        ('auditiva','Auditiva'),
        ('visual','Visual'),
        ],'Discapacidad asignada')
    '''
    ------------------Datos referidos a la persona consultante - Dificultades o limitaciones------------------------------
    '''
    difficulties_or_limitations = fields.Char('Dificultades o limitaciones')
    '''
    ------------------Datos referidos a la persona consultante - Salud------------------------------
    '''    
    condition = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Padece alguna condición que afecte a la salud?')
    chronic_disease = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Enfermedad crónica diagnosticada?')
    dbt = fields.Boolean('Diabetes')
    hta = fields.Boolean('HTA')
    cancer = fields.Boolean('Cáncer')
    erc = fields.Boolean('Enfermedad respiratoria crónica')
    hb = fields.Boolean('Hepatitis B')
    hc = fields.Boolean('Hepatitis C')
    tuberculosis = fields.Boolean('Tuberculosis')
    its = fields.Boolean('ITS')
    vih = fields.Boolean('VIH/SIDA')
    chagas = fields.Boolean('Chagas')
    asma = fields.Boolean('ASMA')
    epoc = fields.Boolean('EPOC')
    renal = fields.Boolean('Enfermedad renal')
    otra = fields.Boolean('Otro')
    treatment = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Recibe tratamiento')
    observations_health = fields.Text('Observaciones sobre salud')
    '''
    ------------------Datos referidos a la persona consultante - Salud mental------------------------------
    ''' 
    actual_psychiatric_treatment = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Recibe tratamiento psiquiátrico actualmente')
    prev_psychiatric_treatment = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Recibio tratamiento psiquiátrico previamente')
    pharmacological_medication = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Consume medicación farmacológica')
    presc_pharmacological_medication = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Consume medicación farmacológica con prescripción')
    profesional = fields.Many2One('party.party','Profesional que prescribe',
        domain= [('is_professional','=',True)])
    administration_institution = fields.Many2One('gnuhealth.institution','Institución que administra la medicación')
    '''
    ------------------Datos referidos a la persona consultante - Situación de violencia------------------------------
    '''
    experiences_of_violence = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Experiencias y vivencias de situaciones de violencia')
    temporality = fields.Selection([
        (None,''),
        ('a','A lo largo de vida y continúa'),
        ('b','A lo largo de su vida y no continúa'),
        ('c','En el pasado'),
        ('d','Actualmente'),
        ],'Temporalidad de la experiencia vivida')
    experiences_type = fields.Selection([
        (None,''),
        ('a','Física'),
        ('b','Psicológica'),
        ('c','Sexual'),
        ('d','Economica y patrimonial'),
        ('e','Simbólica'),
        ],'Tipo de violencia sufrida')
    help_ = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Ayudas, socorros y denuncias')
    observations_violence = fields.Text('Observaciones sobre las situaciones de violencia')
    '''
    ------------------Datos referidos a la persona consultante - Situación ante la ley------------------------------
    '''
    illegal_acts = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Cometió actos que infringen la ley penal')
    quantity_illegal_acts = fields.Selection([
        (None,''),
        ('a','Una ves'),
        ('b','Dos o tres veces'),
        ('c','Cuatro veces o mas'),
        ],'Cantidad de veces que infringió la ley penal')
    arrests = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Ha sido arrestado?')
    quantity_arrests = fields.Selection([
        (None,''),
        ('a','Una ves'),
        ('b','Dos o tres veces'),
        ('c','Cuatro veces o mas'),
        ],'Cantidad de arrestos')
    arrests_for_possession = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Ha sido arrestado por tenencia de sustancia para usos personal?')
    quantity_arrests_for_possession = fields.Selection([
        (None,''),
        ('a','Una ves'),
        ('b','Dos o tres veces'),
        ('c','Cuatro veces o mas'),
        ],'Cantidad de arrestos por tenencia de sustancias para uso personal')
    '''
    ------------------Datos referidos a la persona consultante - Conflictos legales-----------------------------
    '''
    actual_legal_conflict = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Atraviesa un conflicto legal actualmente?')
    exceptional_measure = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Esta implicada en una medida excepcional actualmente?')
    observations_legal_conflict = fields.Text('Observaciones sobre conflictos legales')
    '''
    ------------------Datos referidos a la persona consultante - Situación de ingreso a la CAA------------------------------
    '''
    how_find_out = fields.Selection([
        (None,''),
        ('a','Recomendación de terceros'),
        ('b','Orientó otro centro de atención y acompañamiento de la red de la Sedronar'),
        ('c','Derivó otra institución no dependiente de Sedronar'),
        ('d','Oficio judicial'),
        ('otro','Otras vías de comunicación'),
        ],'Como se entero de la existencia de la CAA?')
    caa_orientation = fields.Selection([
        (None,''),
        ('1','Centro de Atención y Acompañamiento (CAAC)'),
        ('2','Casa convivencial comunitaria (CCC)'),
        ('3','Centro de asistencia inmediata (CAI ex CEDECOR'),
        ('4','Dispositivos territoriales comunitarios (DTC)'),
        ('5','Institución conveniada'),
        ('6','Institución conveniada ambulatorio'),
        ('7','Institución conveniada residencial'),
        ('8','Linea 141 o chat 141'),
        ],'Consignar el tipo de CAA que orientó a la persona?')
    derivation = fields.Char('Nombre de la institución que derivó el caso')
    type_of_reason_for_consultation = fields.Selection([
        (None,''),
        ('1','Realizar tratamiento en la CAA por consumo de sustancias'),
        ('2','Realizar actividades promocionales y/o talleres en el dispositivo'),
        ('3','Orientación legal'),
        ('4','Orientación de salud/salud mental'),
        ('5','Orientación laboral'),
        ('6','Orientación educativa'),
        ('7','Orientación para servicio social'),
        ('8','Orientación para la niñez'),
        ('9','Orientación en problemas comunitarios'),
        ('10','Orientación para internación'),
        ('11','Otro'),
        ],'Tipo de motivo de consulta a la CAA?')
    actual_reason_for_consultation = fields.Selection([
        (None,''),
        ('1','Motivación personal'),
        ('2','Motivado por familia y amigos'),
        ('3','Motivado por la escuela'),
        ('4','Motivado por el trabajo'),
        ('5','Obligado/forzado'),
        ('6','Porque me invitaron a participar'),
        ('7','Otro'),
        ],'Tipo de motivo de consulta a la CAA?')
    expectations = fields.Text('Expectativas', help="Expectativas  en caso de iniciar un tratamiento por consumo")
    '''
    ------------------Datos referidos a la persona consultante - Situación de consumo actual------------------------------
    '''
    current_problematic_substance1 = fields.Selection([
        (None,''),
        ('1','Bebidas alcohólicas'),
        ('2','Tabaco'),
        ('3','Marihuana'),
        ('4','Hashish'),
        ('5','Cocaína'),
        ('6','Pasta base (Paco)'),
        ('7','Crack (Piedra)'),
        ('8','Alita mosca'),
        ('9','Heroína'),
        ('10','Ketamina'),
        ('11','Alucinógenos'),
        ('12','LSD, NBOME, etc.'),
        ('13','Alucinógenos vegetales'),
        ('14','Anfetaminas u otro tipo de estimulantes'),
        ('15','Solventes e inhalables'),
        ('16','Lanzaperfume o Popper'),
        ('17','Medicamentos tranquilizantes s/pm1'),
        ('18','Medicamentos estimulantes s/pm1'),
        ('19','Medicamentos opioides s/pm1'),
        ('20','Medicamentos anticolinérgicos'),
        ('21','Otra'),
        ],'1ra Sustancia problemática actual')
    frequency_of_consumption1 = fields.Selection([
        (None,''),
        ('1','Una o dos veces'),
        ('2','Cada mes'),
        ('3','Cada semana'),
        ('4','Casi a diario'),
        ('5','A diario'),
        ],'Frecuencia de consumo de la sustancia 1 en los últimos 3 meses')
    current_problematic_substance2 = fields.Selection([
        (None,''),
        ('1','Bebidas alcohólicas'),
        ('2','Tabaco'),
        ('3','Marihuana'),
        ('4','Hashish'),
        ('5','Cocaína'),
        ('6','Pasta base (Paco)'),
        ('7','Crack (Piedra)'),
        ('8','Alita mosca'),
        ('9','Heroína'),
        ('10','Ketamina'),
        ('11','Alucinógenos'),
        ('12','LSD, NBOME, etc.'),
        ('13','Alucinógenos vegetales'),
        ('14','Anfetaminas u otro tipo de estimulantes'),
        ('15','Solventes e inhalables'),
        ('16','Lanzaperfume o Popper'),
        ('17','Medicamentos tranquilizantes s/pm1'),
        ('18','Medicamentos estimulantes s/pm1'),
        ('19','Medicamentos opioides s/pm1'),
        ('20','Medicamentos anticolinérgicos'),
        ('21','Otra'),
        ],'2da Sustancia problemática actual')
    frequency_of_consumption2 = fields.Selection([
        (None,''),
        ('1','Una o dos veces'),
        ('2','Cada mes'),
        ('3','Cada semana'),
        ('4','Casi a diario'),
        ('5','A diario'),
        ],'Frecuencia de consumo de la sustancia 2 en los últimos 3 meses')
    current_problematic_substance3 = fields.Selection([
        (None,''),
        ('1','Bebidas alcohólicas'),
        ('2','Tabaco'),
        ('3','Marihuana'),
        ('4','Hashish'),
        ('5','Cocaína'),
        ('6','Pasta base (Paco)'),
        ('7','Crack (Piedra)'),
        ('8','Alita mosca'),
        ('9','Heroína'),
        ('10','Ketamina'),
        ('11','Alucinógenos'),
        ('12','LSD, NBOME, etc.'),
        ('13','Alucinógenos vegetales'),
        ('14','Anfetaminas u otro tipo de estimulantes'),
        ('15','Solventes e inhalables'),
        ('16','Lanzaperfume o Popper'),
        ('17','Medicamentos tranquilizantes s/pm1'),
        ('18','Medicamentos estimulantes s/pm1'),
        ('19','Medicamentos opioides s/pm1'),
        ('20','Medicamentos anticolinérgicos'),
        ('21','Otra'),
        ],'3ra Sustancia problemática actual')
    frequency_of_consumption3 = fields.Selection([
        (None,''),
        ('1','Una o dos veces'),
        ('2','Cada mes'),
        ('3','Cada semana'),
        ('4','Casi a diario'),
        ('5','A diario'),
        ],'Frecuencia de consumo de la sustancia 3 en los últimos 3 meses')
    current_problematic_substance4 = fields.Selection([
        (None,''),
        ('1','Bebidas alcohólicas'),
        ('2','Tabaco'),
        ('3','Marihuana'),
        ('4','Hashish'),
        ('5','Cocaína'),
        ('6','Pasta base (Paco)'),
        ('7','Crack (Piedra)'),
        ('8','Alita mosca'),
        ('9','Heroína'),
        ('10','Ketamina'),
        ('11','Alucinógenos'),
        ('12','LSD, NBOME, etc.'),
        ('13','Alucinógenos vegetales'),
        ('14','Anfetaminas u otro tipo de estimulantes'),
        ('15','Solventes e inhalables'),
        ('16','Lanzaperfume o Popper'),
        ('17','Medicamentos tranquilizantes s/pm1'),
        ('18','Medicamentos estimulantes s/pm1'),
        ('19','Medicamentos opioides s/pm1'),
        ('20','Medicamentos anticolinérgicos'),
        ('21','Otra'),
        ],'4ta Sustancia problemática actual')
    frequency_of_consumption4 = fields.Selection([
        (None,''),
        ('1','Una o dos veces'),
        ('2','Cada mes'),
        ('3','Cada semana'),
        ('4','Casi a diario'),
        ('5','A diario'),
        ],'Frecuencia de consumo de la sustancia 4 en los últimos 3 meses')
    current_problematic_substance5 = fields.Selection([
        (None,''),
        ('1','Bebidas alcohólicas'),
        ('2','Tabaco'),
        ('3','Marihuana'),
        ('4','Hashish'),
        ('5','Cocaína'),
        ('6','Pasta base (Paco)'),
        ('7','Crack (Piedra)'),
        ('8','Alita mosca'),
        ('9','Heroína'),
        ('10','Ketamina'),
        ('11','Alucinógenos'),
        ('12','LSD, NBOME, etc.'),
        ('13','Alucinógenos vegetales'),
        ('14','Anfetaminas u otro tipo de estimulantes'),
        ('15','Solventes e inhalables'),
        ('16','Lanzaperfume o Popper'),
        ('17','Medicamentos tranquilizantes s/pm1'),
        ('18','Medicamentos estimulantes s/pm1'),
        ('19','Medicamentos opioides s/pm1'),
        ('20','Medicamentos anticolinérgicos'),
        ('21','Otra'),
        ],'5ta Sustancia problemática actual')
    frequency_of_consumption5 = fields.Selection([
        (None,''),
        ('1','Una o dos veces'),
        ('2','Cada mes'),
        ('3','Cada semana'),
        ('4','Casi a diario'),
        ('5','A diario'),
        ],'Frecuencia de consumo de la sustancia 5 en los últimos 3 meses')
    combination_in_consumption = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Realizo combinación sustancias los últimos 3 meses')
    '''
    ------------------Datos referidos a la persona consultante - Trayectoria de consumo------------------------------
    '''
    age_of_onset = fields.Integer('Edad de inicio de consumo')
    starting_substance = fields.Char('Sustancia de inicio del consumo')
    consumption_by_injection = fields.Selection([
        (None,''),
        ('si','Si, en los últimos 3 meses'),
        ('si_3','Si, pero no en los últimos 3 meses'),
        ('no','No, nunca'),
        ],'Consumió sustancias por vía inyectada los últimos 3 meses')
    route_of_administration = fields.Selection([
        (None,''),
        ('1','Oral'),
        ('2','Fumada'),
        ('3','Inhalada'),
        ('4','Inyectada'),
        ('5','Absorción por piel o mucosa'),
        ('6','Otro'),
        ],'Vía de administración mas frecuente')
    observations_tray_cons = fields.Text('Observaciones sobre trayectoria de consumo')
    '''
    ------------------Datos referidos a la persona consultante - Trayectoria de tratamientos por consumo------------------------------
    '''
    previous_treatments = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Realizo tratamientos previos por consumo de sustancias?')
    quntitity_previous_treatments = fields.Integer('Cantidad de tratamientos previos por consumo')
    place_where = fields.Many2One('gnuhealth.institution','Lugar')
    '''
    ------------------Datos referidos a la persona consultante - Último tratamiento por consumo------------------------------
    '''
    time_elapsed_since_completion = fields.Char('Tiempo transcurrido desde la finalización del ultimo tratamiento')
    duration_of_last_treatment = fields.Char('Duración del ultimo tratamiento')
    modality_of_last_treatment = fields.Selection([
        (None,''),
        ('1','Ambulatoria'),
        ('2','Residencial'),
        ('3','Comunitaria'),
        ],'Modalidad del ultimo tratamiento')
    reason_for_termination = fields.Selection([
        (None,''),
        ('1','Fue dado de alta, porque terminó el tratamiento'),
        ('2','Fue derivado a otro programa o centro de atención'),
        ('3','Finalizó la beca'),
        ('4','Interrumpió el tratamiento por decisión propia'),
        ('5','Interrumpió el tratamiento por decisión de la institución'),
        ('6','Otro'),
        ],'Motivo de finalización del ultimo tratamiento')
    reason_for_the_interruption = fields.Selection([
        (None,''),
        ('1','Porque considero que ya estaba "recuperado"'),
        ('2','Porque tenía problemas económicos'),
        ('3','Porque tenía problemas familiares'),
        ('4','Porque tenía problemas en el centro de tratamiento'),
        ('5','Porque sentía que no le ayudaba'),
        ('6','Otro'),
        ],'Motivo de la interrupción del ultimo tratamiento')
    '''
    ------------------Datos referidos a la persona consultante - Acompañamiento y apoyo en CAA------------------------------
    '''
    prior_participation = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Participo previamente en CAA que le brindaron contención/apoyo (no tratamiento)')
    current_participation = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Participa actualmente en CAA que le brindaron contención/apoyo (no tratamiento)')
    reason_for_termination_acomp = fields.Selection([
        (None,''),
        ('1','Fue dado de alta, porque terminó el tratamiento'),
        ('2','Fue derivado a otro programa o centro de atención'),
        ('3','Finalizó la beca'),
        ('4','Interrumpió el tratamiento por decisión propia'),
        ('5','Interrumpió el tratamiento por decisión de la institución'),
        ('6','Otro'),
        ],'Motivo de finalización de la participación en CAA')
    reason_for_the_interruption_acomp = fields.Selection([
        (None,''),
        ('1','Porque considero que ya estaba "recuperado"'),
        ('2','Porque tenía problemas económicos'),
        ('3','Porque tenía problemas familiares'),
        ('4','Porque tenía problemas en el centro de tratamiento'),
        ('5','Porque sentía que no le ayudaba'),
        ('6','Otro'),
        ],'Motivo de la interrupción de la participación en CAA')
    '''
    ------------------Datos referidos a la persona consultante - Tratamientos previos por motivos de Salud mental------------------------------
    '''
    previous_treatments_for_mh = fields.Selection([
        (None,''),
        ('si','Si'),
        ('no','No'),
        ],'Realizo tratamientos previos por motivos de salud mental?')
    quntitity_previous_treatments_for_mh = fields.Integer('Cantidad de tratamientos previos por motivos de salud mental')
    place_where_for_mh = fields.Many2One('gnuhealth.institution','Lugar')
    '''
    ------------------Datos referidos a la persona consultante - Último tratamiento por motivos de Salud mental------------------------------
    '''
    time_elapsed_since_completion_mh = fields.Char('Tiempo transcurrido desde la finalización del ultimo tratamiento por motivos de salud mental')
    duration_of_last_treatment_mh = fields.Char('Duración del ultimo tratamiento por motivos de salud mental')
    modality_of_last_treatment_mh = fields.Selection([
        (None,''),
        ('1','Ambulatoria'),
        ('2','Residencial'),
        ('3','Comunitaria'),
        ],'Modalidad del ultimo tratamiento por motivos de salud mental')
    reason_for_termination_mh = fields.Selection([
        (None,''),
        ('1','Fue dado de alta, porque terminó el tratamiento'),
        ('2','Fue derivado a otro programa o centro de atención'),
        ('3','Finalizó la beca'),
        ('4','Interrumpió el tratamiento por decisión propia'),
        ('5','Interrumpió el tratamiento por decisión de la institución'),
        ('6','Otro'),
        ],'Motivo de finalización del ultimo tratamiento por salud mental')
    reason_for_the_interruption_mh = fields.Selection([
        (None,''),
        ('1','Porque considero que ya estaba "recuperado"'),
        ('2','Porque tenía problemas económicos'),
        ('3','Porque tenía problemas familiares'),
        ('4','Porque tenía problemas en el centro de tratamiento'),
        ('5','Porque sentía que no le ayudaba'),
        ('6','Otro'),
        ],'Motivo de la interrupción del ultimo tratamiento por salud mental')
    '''
    ------------------Datos referidos a la persona consultante - Respuesta CAA------------------------------
    '''
    first_answer = fields.Selection([
        (None,''),
        ('1','Se da información'),
        ('2','Se brinda orientación'),
        ('3','Escucha inmediata/manejo de crisis'),
        ('4','Se da una cita'),
        ('5','Acompañamiento'),
        ('6','Cuidados médicos'),
        ('7','Comienzo del proceso de primera escucha'),
        ('8','Acogida/Ingreso'),
        ('9','Se convoca a participar de una actividad/taller'),
        ('10','Derivación'),
        ('11','Modalidad de atención y acompañamiento sugerido (amb/res/común))'),
        ('12','Articulación con otro actor'),
        ('13','Articulación/derivación a otro dispositivo Sedronar'),
        ('14','Otro'),
        ],'Primera respuesta de la CAA')
    name_of_actor = fields.Char('Institución',help="Especifique nombre de la institución o actor con que se articulo/derivo")
    intervening_professionals = fields.Many2One('party.party','Profesionales intervinientes',
        domain= [('is_professional','=',True)])
    observations_first_answer = fields.Text('Observaciones primera respuesta')
    general_observations = fields.Text('Observaciones generales')

    @staticmethod
    def default_estado():
        return 'seguir'

    @staticmethod
    def default_entry_date():
        date_ = date.today()
        return date_

    @staticmethod
    def default_presentation():
        return 'solo'

    @staticmethod
    def default_has_dni():
        return 'si'

    @staticmethod
    def default_registration_date():
        date_ = date.today()
        return date_
    
    @staticmethod
    def default_receiving_user():
        pool = Pool()
        User = pool.get('res.user')
        return User(Transaction().user).id

    @staticmethod
    def default_institution():
        company = Transaction().context.get('company')

        cursor = Transaction().connection.cursor()
        cursor.execute('SELECT party FROM company_company WHERE id=%s \
            LIMIT 1', (company,))
        party_id = cursor.fetchone()
        if party_id:
            cursor = Transaction().connection.cursor()
            cursor.execute('SELECT id FROM gnuhealth_institution WHERE \
                name = %s LIMIT 1', (party_id[0],))
            institution_id = cursor.fetchone()
            if (institution_id):
                return int(institution_id[0])

    @classmethod
    def create(cls, vlist): 
        return super(EntryRegistration, cls).create(vlist)

    @fields.depends('user_')
    def on_change_with_address(self, name=None):
        address_domain = self._get_address_domain()
        if len(address_domain) == 1:
            return address_domain[0]
        return None

    @fields.depends('user_')
    def on_change_with_address_domain(self, name=None):
        return self._get_address_domain()

    def _get_address_domain(self):
        user__addresses_id = []
        if self.user_ and self.user_.addresses:
            user__addresses_id = [x.id for x in self.user_.addresses]
        return user__addresses_id

    @fields.depends('user_')
    def on_change_with_dni(self, name=None):
        if self.user_ and self.user_.ref:
            return self.user_.ref
        else:
            return None

    @fields.depends('user_')
    def on_change_with_cellphone(self, name=None):
        if self.user_:
            return self.user_.phone
        return None

    @fields.depends('user_')
    def on_change_with_dob(self, name=None):
        if self.user_:
            return self.user_.dob
        return None

    @fields.depends('dob')
    def on_change_with_age(self, name=None):
        hoy=date.today().year
        if self.dob:
            dob=self.dob.year
            return int(hoy-dob)
        else:
            return None

    @classmethod
    def set_cellphone(cls, entry_registrations, name, value):
        pool = Pool()
        ContactMechanism = pool.get('party.contact_mechanism')
        for s in entry_registrations:
            if not s.user_:
                continue
            party_phone = ContactMechanism.search([
                ('party', '=', s.user_),
                ('type', '=', 'phone'),
                ('value', '=', s.user_.phone)
                ], limit=1, order=[('create_date', 'ASC')])
            if party_phone and party_phone[0].value != value:
                ContactMechanism.write(party_phone, {
                    'value':value,
                    })
            if not party_phone and value != None:
                ContactMechanism.create([{
                    'party': s.user_.id,
                    'type': 'phone',
                    'value': value,
                    }])


    @classmethod
    def view_attributes(cls):
        return super(EntryRegistration, cls).view_attributes() + [
            ('/form/notebook/page/group[@id="personal_3ro"]', 'states', {
                'invisible': Eval('presentation').in_(['solo', 'acompañado',None])
                })]+[
            ('/form/group/group/group[@id="hijos_data"]', 'states', {
                'invisible': Eval('has_children').in_(['no',None])
                }),
            ]+[
            ('/form/group/group/group[@id="dni_group"]', 'states', {
                'invisible': Eval('has_dni').in_(['no',None])
                }),
            ]
    
    @classmethod
    def __setup__(cls):
        super(EntryRegistration, cls).__setup__()
        cls._order.insert(0, ('entry_date', 'DESC'))

        cls._buttons.update({
                'crear_seguimiento': {
                    'invisible': (~Eval('estado').in_(
                        ['seguir'])
                        ),
                    'depends': ['estado']
                    }
                })

    @classmethod
    @ModelView.button_action('contact_tracing_ptis.crear_seguimiento_wizard')
    def crear_seguimiento(cls, entry_registrations):
        pass

