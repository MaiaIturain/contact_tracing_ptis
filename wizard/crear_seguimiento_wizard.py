from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Eval
from .exceptions import RateError
from trytond.i18n import gettext

from datetime import date, datetime


class CrearSeguimientoStart(ModelView):
    'Crear Seguimiento'
    __name__ = 'contact_tracing_ptis.crear_seguimiento.start'

    entry_registration = fields.Many2One('contact_tracing_ptis.entry_registration', 'Registro de ingreso',
        required=True,
        readonly=True)
    institution = fields.Many2One('company.company','Institución')
    contact_tracings = fields.One2Many('contact_tracing_ptis.contact_tracing_ptis_call',
        'name','Contact Tracing')
    first_contact = fields.DateTime('Fecha de registro')
    start_of_treatment = fields.DateTime('Inicio del tratamiento')
    

class CrearSeguimientoWizard(Wizard):
    'Crear Seguimiento - Wizard'
    __name__ = 'contact_tracing_ptis.crear_seguimiento.wizard'

    start = StateView('contact_tracing_ptis.crear_seguimiento.start',
            'contact_tracing_ptis.crear_seguimiento_start_form_view',[
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Crear Seguimiento', 'crear_seguimiento', 'tryton-ok', default=True),
                ])

    crear_seguimiento = StateTransition()

    def default_start(self, fields):
        res = {}
        pool = Pool()
        entry_registration_id = Transaction().context.get('active_id', None)
        Company = pool.get('company.company')
        company = Company(Transaction().context.get('company'))
        res ={
               'entry_registration':entry_registration_id,
               'institution': company.id,
               'contact_tracings':None,
               'first_contact':datetime.now(),
               'start_of_treatment':datetime.now(),
                }

        return res

    def transition_crear_seguimiento(self):
        pool = Pool()
        Contact_tracing = pool.get('contact_tracing_ptis.contact_tracing.ptis')
        contact_tracing = Contact_tracing.search(['entry_registration.id', '=', self.start.entry_registration.id])
        if len(contact_tracing) == 0: 
            contact_tracings = []
            contact_tracing_ptis = {}
            contact_tracing_ptis ={
                'entry_registration': self.start.entry_registration.id,
                'institution': self.start.institution.id,
                'contact_tracings':None,
                'first_contact':self.start.first_contact,
                'start_of_treatment':self.start.start_of_treatment,
                        }
            contact_tracings.append(contact_tracing_ptis)
            Contact_tracing.create(contact_tracings)
        else:
            raise RateError(gettext('contact_tracing_ptis.msg_already_exists'))
        return 'end'