from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval

from datetime import datetime
from decimal import Decimal
import operator

class HealthInstitution(metaclass=PoolMeta):
    'Health Institution'
    __name__ = 'gnuhealth.institution'

    
    id_ = fields.Numeric('ID')
    type_ca = fields.Selection([
        (None, ''),
        ('caac', 'CAACs'),
        ('ccc', 'CCC'),
        ('cai', 'CAI'),
        ('dtc','DTC'),
        ('ic','IC'),
        ('ambulatorio','Centro conveniado ambulatorio'),
        ('residencial','Centro conveniado residencial')],'Tipo de centro de atención')
    monday = fields.Boolean('Lunes')
    time_start_mon = fields.Time('Hora de Inicio', format='%H:%M')
    time_end_mon = fields.Time('Hora de Fin', format='%H:%M')
    tuesday = fields.Boolean('Martes')
    time_start_tue = fields.Time('Hora de Inicio', format='%H:%M')
    time_end_tue = fields.Time('Hora de Fin', format='%H:%M')
    wednesday = fields.Boolean('Miércoles')
    time_start_wed = fields.Time('Hora de Inicio', format='%H:%M')
    time_end_wed = fields.Time('Hora de Fin', format='%H:%M')
    thursday = fields.Boolean('Jueves')
    time_start_thu = fields.Time('Hora de Inicio', format='%H:%M')
    time_end_thu = fields.Time('Hora de Fin', format='%H:%M')
    friday = fields.Boolean('Viernes')
    time_start_fri = fields.Time('Hora de Inicio', format='%H:%M')
    time_end_fri = fields.Time('Hora de Fin', format='%H:%M')
    saturday = fields.Boolean('Sábado')
    time_start_sat = fields.Time('Hora de Inicio', format='%H:%M')
    time_end_sat = fields.Time('Hora de Fin', format='%H:%M')
    sunday = fields.Boolean('Domingo')
    time_start_sun = fields.Time('Hora de Inicio', format='%H:%M')
    time_end_sun = fields.Time('Hora de Fin', format='%H:%M')
    dir_medica = fields.Char('Dirección medica')
    dir_terapeutica = fields.Char('Dirección terapeutica')
    dir_tecnica = fields.Char('Dirección tecnica')

    @classmethod
    def view_attributes(cls):
        return super(HealthInstitution, cls).view_attributes() + [
            ('/form/group/group[@id="caa_info"]', 'states', {
                'invisible': ~Eval('institution_type').in_(['caa'])
                })]

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.institution_type.selection.append(('caa','Casa de Atención y Acompañamiento (CAA)'))
        cls.institution_type.selection.append(('school','Educativa'))
        cls.institution_type.selection.append(('sport','Deportiva'))
