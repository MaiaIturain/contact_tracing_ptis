# -*- coding: utf-8 -*-
import urllib.parse
import sys

from trytond.model import ModelView, ModelSQL, Workflow, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Or, Eval, Not, Bool, Equal
from trytond import backend
from trytond.i18n import gettext

from trytond.transaction import Transaction
from trytond.tools import reduce_ids, grouped_slice
#from .exceptions import PatientCurrentlyContacted

from sql import *
from sql.aggregate import *

from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date


class ContactTracingPtis(ModelSQL, ModelView):
    'Contactos con la persona'
    __name__ = 'contact_tracing_ptis.contact_tracing.ptis'

    institution = fields.Many2One('company.company','Institución')
    user_ = fields.Many2One('party.party','Solicitante')
    contact_tracings = fields.One2Many('contact_tracing_ptis.contact_tracing_ptis_call',
        'name','Contactos')
    first_contact = fields.DateTime('Fecha de registro')
    start_of_treatment = fields.DateTime('Inicio del tratamiento')
    entry_registration = fields.Many2One('contact_tracing_ptis.entry_registration','Registro de ingreso',readonly=True)

    @classmethod
    def create(cls, vlist): 
        return super(ContactTracingPtis, cls).create(vlist)

    @staticmethod
    def default_user_():
        pool = Pool()
        Entry_registration = pool.get('contact_tracing_ptis.entry_registration')
        entry_registration = {}
        entry_registration_id = Transaction().context.get('active_id', None)
        entry_registration = Entry_registration.search(['id', '=', entry_registration_id])
        return entry_registration[0].user_ if entry_registration else None


class ContactTracingPtisCall(ModelSQL, ModelView,Workflow):
    'Llamadas'
    __name__ = 'contact_tracing_ptis.contact_tracing_ptis_call'

    name = fields.Many2One('contact_tracing_ptis.contact_tracing.ptis','Contactos')
    caller_tracing  = fields.Many2One('gnuhealth.healthprofessional','Quien contacta?')
    date_ = fields.Date('Fecha')
    current_situation = fields.Text('Intervención actual')

    @staticmethod
    def default_date_():
        now = date.today()
        return now

class ContactTracingName(ModelSQL, ModelView,Workflow):
    'Contact tracing name'
    __name__ = 'contact_tracing_ptis.name'

    name = fields.Char('Nombre')
    desc = fields.Char('Descripción')


