from trytond.pool import Pool
from . import entry_registration
from . import contact_tracing_ptis
from . import health

from .wizard import crear_seguimiento_wizard

def register():
    Pool.register(
        entry_registration.EntryRegistration,
        health.HealthInstitution,
        contact_tracing_ptis.ContactTracingPtis,
        contact_tracing_ptis.ContactTracingPtisCall,
        contact_tracing_ptis.ContactTracingName,
        crear_seguimiento_wizard.CrearSeguimientoStart,
        module='contact_tracing_ptis', type_='model')
    Pool.register(
        crear_seguimiento_wizard.CrearSeguimientoWizard,
        module='contact_tracing_ptis', type_='wizard')

      


